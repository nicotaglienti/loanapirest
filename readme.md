Backend Developer Challenge
The purpose of this challenge is to test your ability to implement a solution given an abstract problem.
We want you to develop a RESTFull API application which a big bank can easily issue new loans and find out what the value and volume of
outstanding debt are.
Problem:
A big bank needs to keep track of the amount of money loaned and the missed/made payments.
A big bank needs a place to retrieve the volume of outstanding debt at some point in time.
Limitations:
Loans are paid back in monthly installments.
Rules of the challenge
It should be solved on a weekend (3 days).
The solution should take between 8hr to 12hr.
The project needs to be published on github personal repository.
The solution will be presented in front of the Development Team 1 or 2 days after the presentation.
Tech Stack to be used on the solution (One of them):
NodeJs (preferred)
Laravel
Symfony
Tools/Suggestions:
Postman: to create the endpoints to test the API. https://www.getpostman.com/
NodeJs Framework: https://sailsjs.com/
Endpoints
POST /loans
Summary
Creates a loan application. Loans are automatically accepted.
Payload
amount: loan amount in dollars.
term: number of months that will take until the loan gets paid-off.
rate: interest rate as decimal.
date: when the loan was requested (origination date as an ISO 8601 string).
Example
{
"amount": 1000,
"term": 12,
"rate": 0.05,
"date": "2017-08-05 02:18Z",
}
Response
loan_id: unique id of the loan.
installment: monthly loan payment.
Example
{
"loan_id": "000-0000-0000-0000",
"installment": 85.60
}
Notes
Loan payment formula
r = rate / 12.
Installment (monthly) = [ r + r / ( (1+r) ^ term - 1) ] x amount
Example
For repaying a loan of $1000 at 5% interest for 12 months, the equation would be:
Installment (monthly) = [ (0.05 / 12) + (0.05 / 12) / ( (1+ (0.05 / 12)) ^ 12 -1) ] x 1000
Installment (monthly) = $85.60
GET /loans
Summary
Get a list of loans, allowing to filter based on dates.
Query String
from: from date.
to: to date.
Response
Example
{
[
{
"id": 1,
"amount": 1000,
"term": 12,
"rate": 0.05,
"date": "2017-08-05 02:18Z",
},
{
"id": 2,
"amount": 1000,
"term": 12,
"rate": 0.05,
"date": "2017-08-05 02:18Z",
}
]
}
POST /loans/<:id>/payments
Summary
Creates a record of a payment made or missed.
Payload
payment: type of payment: made or missed.
date: payment date.
amount: amount of the payment made or missed in dollars.
Example (Payment made)
{
 "payment": "made",
 "date": "2017-09-05 02:18Z",
 "amount": 85.60,
}
Example (Payment missed)
{
 "payment": "missed",
 "date": "2017-09-05 02:18Z",
 "amount": 85.60,
}
GET /loans/<:id>/balance
Summary
Get the volume of outstanding debt (i.e., debt yet to be paid) at some point in time.
Payload
date: loan balance until this date.
Example
{
 "date": "2017-09-05 02:18Z"
}
Response
balance: outstanding debt of loan.
Example
{
"balance": 40
}
Optional Endpoints
These endpoints is to add security and also to allow multiple loans for different users. If a user request a loan, he needs to send the token on
every request, so he can receive their only personal loans.
POST /users
Summary
Register a user and return the created user.
Payload
first_name: First name
last_name: Last name
email: An email
password: User password
Example
{
"first_name": German,
"last_name": Ocampo,
"email": german.ocampo@endeev.com,
"password": "AReallyComplexPassword%",
}
POST /login
Summary
Login a user with email and password.
Payload
email: An email
password: User password
Example
{
"email": german.ocampo@endeev.com,
"password": "AReallyComplexPassword%",
}
