<?php
namespace App\Http\Controllers\Api;

use App\Payment;
use App\Loan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $response = Payment::search($request->query());
        return response()->json($response);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return Response::json([
            'message' => 'not implemented yet'
        ], 501);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request, int $id) {
        $this->validate($request, Payment::$validationRules);
        $user = Auth::user();
        $requestData = $request->all();
        $requestData['loan_id'] = $id;
        $requestData['payment'] = Payment::validatePayment($requestData['payment']);
        if ($requestData['payment']==false) {
            return response()->json([
                "message"=> "The given data was invalid.",
                "errors"=> [
                    "payment"=> [
                        "invalid string given, must be equal to 'made' or 'missed'"
                    ]
                ]
            ], 422);
        }
        if (!Loan::validateUserAuth($id, $user->id)) {
            return response()->json([
                'message'=>'loan not found'
            ], 404);
        }
        $payment = Payment::create($requestData);
        return response()->json($payment, 201);
    }
    /**
    * Get the volume of outstanding debt (i.e., debt yet to be paid)
    * at some point in time.
    * @param  Request $request [description]
    * @param  int     $id      [description]
    * @return [type]           [description]
    */
    public function getBalance(Request $request, int $id) {
        $user = Auth::user();
        if (!Loan::validateUserAuth($id, $user->id)) {
            return response()->json([
                'message'=>'loan not found'
            ], 404);
        }
        $query = $request->query();
        $query['loan_id'] = $id;
        $query['user_id'] = $user->id;
        $response = Payment::getBalance($query);
        return response()->json($response);
    }

}
