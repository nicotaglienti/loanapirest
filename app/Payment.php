<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Payment extends Model
{
   /**
   * The table associated with the model.
   *
   * @var string
   */
   protected $table = 'payments';

   /**
   * The database primary key value.
   *
   * @var string
   */
   protected $primaryKey = 'id';

   /** The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = ['amount', 'payment', 'loan_id', 'date'];

   /**
   * validation rules
   *
   * @var array
   */
   public static $validationRules = [
       'amount' => 'required|numeric',
       'payment' => 'required|string',
       'date' => 'required'
   ];


   public function loan() {
      return $this->belongsTo('App\Loan');
   }

      /**
      * Set the date formated.
      *
      * @param  string  $value
      * @return void
      */
      public function setDateAttribute($value) {
         $this->attributes['date'] = date('Y-m-d H:i:s', strtotime($value));
      }

      /**
      * Get the date formated.
      *
      * @param  string  $value
      * @return string
      */
      public function getDateAttribute($value) {
         return date(DATE_ISO8601, strtotime($value));
      }

      public static function validatePayment($payment){
         $payment = strtoupper($payment);
         if ($payment != 'MISSED' && $payment != 'MADE') {
            return false;
         }
         return $payment;
      }
      public static function getBalance($query){
         $data = DB::table('payments')->where('payment', '=', 'MISSED');

         if (array_key_exists('date', $query)) {
            $data->where('date', '<=', date('Y-m-d H:i:s', strtotime($query['date'])));
         }
         if (array_key_exists('loan_id', $query)) {
            $data->where('loan_id', '=', $query['loan_id']);
         } else {
            return false;
         }
         $missedPayments = $data->get();
         $balance = 0;
         foreach ($missedPayments as $key => $value) {
               $balance+=$value->amount;
         }
         return ['balance'=> $balance];
      }




}
