<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Loan extends Model
{
   /**
   * The table associated with the model.
   *
   * @var string
   */
   protected $table = 'loans';

   /**
   * The database primary key value.
   *
   * @var string
   */
   protected $primaryKey = 'id';

   /** The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = ['amount', 'term', 'rate', 'date', 'user_id'];

   /**
   * validation rules
   *
   * @var array
   */
   public static $validationRules = [
       'amount' => 'required|numeric',
       'term' => 'required|numeric',
       'rate' => 'required',
       'date' => 'required'
   ];

   public function payment() {
      return $this->hasMany('App\Payment');
   }


   /**
   * Set the date formated.
   *
   * @param  string  $value
   * @return void
   */
   public function setDateAttribute($value) {
      $this->attributes['date'] = date('Y-m-d H:i:s', strtotime($value));
   }

   /**
   * Get the date formated.
   *
   * @param  string  $value
   * @return string
   */
   public function getDateAttribute($value) {
      return date(DATE_ISO8601, strtotime($value));
   }



   /**
   * [getInstallment description]
   * @return [type] [description]
   */
   public function getInstallment(){
      $r = $this->rate / 12;
      return  round((( $r + $r / ( (1+$r) ** $this->term - 1) ) * $this->amount), 2);
   }

   /**
    * [search description]
    * @param  array  $query [description]
    * @return [type]        [description]
    */
   public static function search(array $query=[]){
      $response = DB::table('loans');
      if (array_key_exists('from', $query)) {
         $response->where('date', '>=', date('Y-m-d H:i:s', strtotime($query['from'])));
      }
      if (array_key_exists('to', $query)) {
         $response->where('date', '<=', date('Y-m-d H:i:s', strtotime($query['to'])));
      }
      if (array_key_exists('user_id', $query)) {
         $response->where('user_id', '=', $query['user_id']);
      }
      return $response->get(['amount', 'term', 'rate', 'date']);
   }

   public static function validateUserAuth($loanId = 0, $userId = 0){
      $response = DB::table('loans');
      $response->where('id', '=', $loanId);
      $response->where('user_id', '=', $userId);
      return $response->count();
   }


}
