<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('users', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('profile', 'Api\AuthController@profile');
    Route::resource('loans', 'Api\LoanController');

    Route::post('loans/{id}/payments', 'Api\PaymentController@store');
    Route::get('loans/{id}/balance', 'Api\PaymentController@getBalance');
});
